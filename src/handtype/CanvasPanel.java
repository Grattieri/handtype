/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handtype;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.util.Random;

/**
 *
 * @author massimo
 */
public class CanvasPanel extends javax.swing.JPanel {

    /**
     * Creates new form CanvasPanel
     */
   
   int x;
   int y;
   int lx;
   int ly;
   boolean start=true ;
   Graphics2D g2d;
   
    public void pointDraw( int x, int y ) { 
       this.x=x;
       this.y=y;
       repaint();
    } 
    public void pointMove( int x, int y ) { 
       this.x=x;
       this.y=y;
       this.lx=x;
       this.ly=y;
    } 
    
    

    @Override
    public void paintComponent(Graphics g) {
       g2d = (Graphics2D) g;
       g2d.setColor(Color.blue);
       g2d.setStroke(new BasicStroke(10.0f));
       g2d.drawLine(lx, ly-28, x, y-28);
       this.lx=x;
       this.ly=y;
       if (start){
           start=false;
           //super.paintComponent(g);
       }
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setForeground(new java.awt.Color(255, 204, 204));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
